module e/account

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/johnnadratowski/golang-neo4j-bolt-driver v0.0.0-20200323142034-807201386efa
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
	github.com/valyala/fasttemplate v1.1.0 // indirect
	go.mongodb.org/mongo-driver v1.3.2
)
