package neo4j

import (
	"fmt"

	"../../models/entities"
	"../../utils/config"

	bolt "github.com/johnnadratowski/golang-neo4j-bolt-driver"
)

// 1 index for mongoid in neo4j should be created
const (
	followers  = "MATCH (u:USER {mongoId: $userMongoId}) MATCH  (fu:USER)-[:FOLLOW]->(u) return fu.mongoId, fu.name, fu.avatarURL SKIP $itemsToSkip  LIMIT $limit"
	followings = "MATCH (u:USER {mongoId: $userMongoId}) MATCH  (u)-[:FOLLOW]->(fu:USER) return fu.mongoId, fu.name, fu.avatarURL SKIP $itemsToSkip  LIMIT $limit"
)

// GetFollowers create and return new Event record
func GetFollowers(userID string, itemsToSkip int64, itemsPerPage int64) ([]*entities.Follower, error) {
	var con, err = createConnection()

	if err != nil {
		return nil, err
	}

	defer con.Close()

	rows, err := con.QueryNeo(followers, map[string]interface{}{
		"userMongoId": userID,
		"itemsToSkip": itemsToSkip,
		"limit":       itemsPerPage,
	})

	if err != nil {
		return nil, err
	}

	data, _, err := rows.All()

	if err != nil {
		return nil, err
	}

	var result []*entities.Follower

	for _, nodeSet := range data {
		if nodeSet != nil {
			var f = new(entities.Follower)

			f.UserID = nodeSet[0].(string)
			if nodeSet[1] != nil {
				f.UserName = nodeSet[1].(string)
			}

			if nodeSet[2] != nil {
				f.AvatarURL = nodeSet[2].(string)
			}

			f.IsMyFollowing = nodeSet[3].(bool)

			result = append(result, f)
		}
	}

	return result, nil
}

// GetFollowings create and return new Event record
func GetFollowings(userID string, itemsToSkip int64, itemsPerPage int64) ([]*entities.Follower, error) {
	var con, err = createConnection()

	if err != nil {
		return nil, err
	}

	defer con.Close()

	rows, err := con.QueryNeo(followings, map[string]interface{}{
		"userMongoId": userID,
		"itemsToSkip": itemsToSkip,
		"limit":       itemsPerPage,
	})

	if err != nil {
		return nil, err
	}

	data, _, err := rows.All()

	if err != nil {
		return nil, err
	}

	var result []*entities.Follower

	for _, nodeSet := range data {
		if nodeSet != nil {
			var f = new(entities.Follower)

			f.UserID = nodeSet[0].(string)
			if nodeSet[1] != nil {
				f.UserName = nodeSet[1].(string)
			}

			if nodeSet[2] != nil {
				f.AvatarURL = nodeSet[2].(string)
			}

			f.IsMyFollowing = nodeSet[3].(bool)

			result = append(result, f)
		}
	}

	return result, nil
}

func createConnection() (bolt.Conn, error) {

	driver := bolt.NewDriver()

	con, err := driver.OpenNeo(getConnectionString())

	if err != nil {
		return nil, err
	}

	con.SetTimeout(config.GetNeo4JConnectionTimeOut())

	return con, nil
}

func getConnectionString() string {
	return fmt.Sprintf("bolt://%s:%s@%s", config.GetNeo4JUser(), config.GetNeo4JPassword(), config.GetNeo4JHost())
}
