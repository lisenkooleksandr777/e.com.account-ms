package graphsynch

import (
	"fmt"
	"time"

	"e/account/models/entities"
	"e/account/utils/config"

	bolt "github.com/johnnadratowski/golang-neo4j-bolt-driver"
)

// 1 index for mongoid in neo4j should be created
const (
	createEvent = "MERGE (u:USER {mongoId: $userMongoId }) " +
		"MERGE (l:LOCATION { long: $long, lat: $lat, name: $locationName, type: $locationType, detailedLocation: $detailedLocation, url: $locationURL}) " +
		"CREATE(e: EVENT {mongoId: $eventMongoId, dateStart: $dateStart, dateEnd: $dateEnd, name: $name, logo: $logo, isPublic: $isPublic }) " +
		"CREATE (e)-[el:LOCATION]->(l) " +
		"CREATE (u)-[r:OWNER]->(e) "

	deleteEvent = "match (e:EVENT {mongoId: $mongoId}) detach delete e"
)

// AddToGraph create and return new Event record
func AddToGraph(event *entities.Event) error {
	var con, err = createConnection()
	defer con.Close()

	if err != nil {
		return err
	}

	_, err = con.ExecNeo(createEvent, map[string]interface{}{
		"userMongoId": event.OwnerID,

		"long":             event.Location.Coordinates[0],
		"lat":              event.Location.Coordinates[1],
		"locationName":     event.Location.Name,
		"locationType":     event.Location.Type,
		"detailedLocation": event.Location.DetailedLocation,
		"locationURL":      event.Location.URL,

		"eventMongoId": event.ID.Hex(),
		"dateStart":    event.DateStart.Format(time.RFC3339),
		"dateEnd":      event.DateCreated.Format(time.RFC3339),
		"name":         event.Name,
		"logo":         event.LogoURL,
		"isPublic":     event.IsPublic,
	})
	return err

}

// DeleteEventFromGraph and relations
func DeleteEventFromGraph(eventID string) error {
	var con, err = createConnection()

	if err != nil {
		return err
	}

	defer con.Close()

	_, err = con.ExecNeo(deleteEvent, map[string]interface{}{
		"mongoId": eventID,
	})
	return err
}

func createConnection() (bolt.Conn, error) {

	driver := bolt.NewDriver()

	con, err := driver.OpenNeo(getConnectionString())

	if err != nil {
		return nil, err
	}

	con.SetTimeout(config.GetNeo4JConnectionTimeOut())

	return con, nil
}

func getConnectionString() string {
	return fmt.Sprintf("bolt://%s:%s@%s", config.GetNeo4JUser(), config.GetNeo4JPassword(), config.GetNeo4JHost())
}
