package event

import (
	"context"
	"errors"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	bson_ "go.mongodb.org/mongo-driver/bson"

	"e/account/models/entities"
	"e/account/utils/config"
)

const (
	eventsCollection = "events"

	geolocationTypePoint = "Point"
)

// Create create and return new Event record
func Create(request *entities.NewEvent) (*entities.Event, error) {

	request.DateCreated = time.Now()
	request.Location.Type = geolocationTypePoint

	mongoClient, err := createConnection()

	if err != nil {
		return nil, err
	}

	defer mongoClient.Disconnect(context.TODO())

	var collection = mongoClient.Database("e_com").Collection(eventsCollection)
	insertResult, err := collection.InsertOne(context.Background(), request, options.InsertOne())

	if err != nil {
		return nil, err
	}

	res := collection.FindOne(context.Background(), bson_.D{{"_id", insertResult.InsertedID}}, options.FindOne())

	var result entities.Event
	err = res.Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil

	return nil, errors.New("Error during event creating")
}

// Delete create and return new Event record
func Delete(ownerID string, eventID string) (*entities.Event, error) {

	objectID, err := primitive.ObjectIDFromHex(eventID)

	if err != nil {
		return nil, err
	}

	mongoClient, err := createConnection()

	if err != nil {
		return nil, err
	}

	defer mongoClient.Disconnect(context.TODO())

	var collection = mongoClient.Database("e_com").Collection(eventsCollection)

	res := collection.FindOne(context.Background(), bson_.D{{"_id", objectID}, {"ownerID", ownerID}}, options.FindOne())

	var event entities.Event
	err = res.Decode(&event)
	if err != nil {
		return nil, err
	}

	deleteRes, err := collection.DeleteOne(context.Background(), bson_.D{{"_id", objectID}, {"ownerID", ownerID}}, options.Delete())

	if err != nil {
		return nil, err
	}
	if deleteRes.DeletedCount == 1 {
		return &event, nil
	}

	return nil, errors.New("Error during deletion of event record")
}

// OwnEvents return page for event created by account owner
func OwnEvents(ownerID string, page int64, itemsPerPage int64) ([]entities.PreviewEvent, error) {

	mongoClient, err := createConnection()

	if err != nil {
		return nil, err
	}

	defer mongoClient.Disconnect(context.TODO())

	var collection = mongoClient.Database("e_com").Collection(eventsCollection)

	skip := page * itemsPerPage
	cur, err := collection.Find(context.Background(),
		bson_.M{"ownerID": ownerID},
		&options.FindOptions{Limit: &itemsPerPage, Skip: &skip, Sort: bson_.D{{"dateStart", -1}}})

	if err == nil {
		defer cur.Close(context.Background())
		var result []entities.PreviewEvent
		for cur.Next(context.Background()) {
			var currentItem entities.PreviewEvent

			err := cur.Decode(&currentItem)
			if err != nil {
				return nil, err
			}
			result = append(result, currentItem)
		}
		return result, nil
	}

	return nil, errors.New("Connection problem")
}

func createConnection() (*mongo.Client, error) {

	client, err := mongo.Connect(context.TODO(),
		options.Client().ApplyURI(config.GetConnectionString()).SetConnectTimeout(config.GetMongoConnectionTimeOut()))

	if err != nil {
		log.Fatal(err)
	}

	return client, err
}
