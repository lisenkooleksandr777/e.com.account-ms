package consumer

import (
	"encoding/json"
	"errors"

	"github.com/streadway/amqp"

	"e/account/models/entities"
	"e/account/repositories/graphsynch"
	"e/account/utils/config"

	"e/account/services/cleaner"
)

const (
	//CreatedEventsTopicName topic of created events which should be synchronized
	createdEventsTopicName = "created_events"

	//DeletedEventsTopicName topic of deleted events which should be synchronized, resources need to be delete
	//files, attachmentd, etc
	deletedEventsTopicName = "deleted_events"
)

//Init initialization of consumer package
func Init() {
	go initCreatedEventsConsumer()
	go initDeletedEventsConsumer()
}

func initCreatedEventsConsumer() {
	connection, err := amqp.Dial(config.GetRabbitMQConnectionString())

	if err != nil {
		panic(errors.New("could not establish connection with RabbitMQ:" + err.Error()))
	}

	channel, err := connection.Channel()

	if err != nil {
		panic(errors.New("could not open RabbitMQ channel:" + err.Error()))
	}

	// We consume data from the queue named Test using the channel we created in go.
	msgs, err := channel.Consume(createdEventsTopicName, "", false, false, false, false, nil)

	if err != nil {
		panic(errors.New("error consuming the queue: " + err.Error()))
	}

	// We loop through the messages in the queue and print them in the console.
	// The msgs will be a go channel, not an amqp channel
	for msg := range msgs {

		var event entities.Event
		err := json.Unmarshal(msg.Body, &event)

		if err == nil {
			err := graphsynch.AddToGraph(&event)
			if err == nil {
				msg.Ack(false)
			}
		}
	}

	// We close the connection after the operation has completed.
	defer connection.Close()
}

func initDeletedEventsConsumer() {
	connection, err := amqp.Dial(config.GetRabbitMQConnectionString())

	if err != nil {
		panic(errors.New("could not establish connection with RabbitMQ:" + err.Error()))
	}

	channel, err := connection.Channel()

	if err != nil {
		panic(errors.New("could not open RabbitMQ channel:" + err.Error()))
	}

	// We consume data from the queue named Test using the channel we created in go.
	msgs, err := channel.Consume(deletedEventsTopicName, "", false, false, false, false, nil)

	if err != nil {
		panic(errors.New("error consuming the queue: " + err.Error()))
	}

	// We loop through the messages in the queue and print them in the console.
	// The msgs will be a go channel, not an amqp channel
	for msg := range msgs {

		var event entities.Event
		err := json.Unmarshal(msg.Body, &event)

		if err == nil {
			err := cleaner.DeleteEvent(&event)
			if err == nil {
				msg.Ack(false)
			}
		}
	}

	// We close the connection after the operation has completed.
	//defer connection.Close()
}
