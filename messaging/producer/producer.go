package producer

import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/streadway/amqp"

	"e/account/utils/config"

	"e/account/models/entities"
)

const (
	//CreatedEventsTopicName topic of created events which should be synchronized
	createdEventsTopicName = "created_events"

	//DeletedEventsTopicName topic of deleted events which should be synchronized, resources need to be delete
	//files, attachmentd, etc
	deletedEventsTopicName = "deleted_events"

	//FilesToDeleteTopicName topic of deleted events which should be synchronized, resources need to be delete
	//files, attachmentd, etc
	filesToDeleteTopicName = "files_to_delete"
)

var channel *amqp.Channel

//Init initialization of producer package
func Init() {
	connection, err := amqp.Dial(config.GetRabbitMQConnectionString())

	if err != nil {
		panic(errors.New("Could not establish connection with RabbitMQ:" + err.Error()))
	}

	// Create a channel from the connection. We'll use channels to access the data in the queue rather than the
	// connection itself
	ch, err := connection.Channel()

	if err != nil {
		panic(errors.New("Could not open RabbitMQ channel:" + err.Error()))
	}

	q, err := ch.QueueDeclare(
		deletedEventsTopicName,
		true,
		false,
		false,
		false,
		nil)

	if err != nil {
		panic(errors.New("Could not declar RabbitMQ que for deleted events:" + err.Error()))
	} else {
		fmt.Println(fmt.Sprintf("Changel %s was declared.", q.Name))
	}

	cq, err := ch.QueueDeclare(
		createdEventsTopicName,
		true,
		false,
		false,
		false,
		nil)

	if err != nil {
		panic(errors.New("Could not declar RabbitMQ que for created events:" + err.Error()))
	} else {
		fmt.Println(fmt.Sprintf("Changel %s was declared.", cq.Name))
	}

	cf, err := ch.QueueDeclare(
		filesToDeleteTopicName,
		true,
		false,
		false,
		false,
		nil)

	if err != nil {
		panic(errors.New("Could not declar RabbitMQ que for created events:" + err.Error()))
	} else {
		fmt.Println(fmt.Sprintf("Changel %s was declared.", cf.Name))
	}

	channel = ch
}

//PublishFileToDeleteQue - publishing file to delete
func PublishFileToDeleteQue(fileName string) error {
	// We create a message to be sent to the queue.
	// It has to be an instance of the aqmp publishing struct
	message := amqp.Publishing{
		Body: []byte(fileName),
	}

	return channel.Publish("", filesToDeleteTopicName, false, false, message)
}

//PublishEventToDeletedQue - publishing file to delete
func PublishEventToDeletedQue(event *entities.Event) error {
	// We create a message to be sent to the queue.
	// It has to be an instance of the aqmp publishing struct
	body, err := json.Marshal(event)

	if err != nil {
		return err
	}

	message := amqp.Publishing{
		Body: body,
	}

	return channel.Publish("", deletedEventsTopicName, false, false, message)
}

//PublishEventToCreatedQue - publishing file to delete
func PublishEventToCreatedQue(event *entities.Event) error {
	// We create a message to be sent to the queue.
	// It has to be an instance of the aqmp publishing struct
	body, err := json.Marshal(event)

	if err != nil {
		return err
	}

	message := amqp.Publishing{
		Body: body,
	}

	return channel.Publish("", createdEventsTopicName, false, false, message)
}
