package event

import (
	"errors"
	"net/http"
	"strconv"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"

	auth "e/account/models/auth"
	"e/account/services/event"

	entities "e/account/models/entities"
)

//CreateEvent endpoint for POST method of event creation
func CreateEvent(c echo.Context) error {

	user := c.Get("user").(*jwt.Token)

	if user == nil {
		return c.JSON(http.StatusUnauthorized, errors.New("Authentication data is not correct. "))
	}
	claims := user.Claims.(*auth.JwtUserClaims)
	if claims == nil {
		return c.JSON(http.StatusForbidden, errors.New("User claims are absent. "))
	}
	var eventRequest entities.NewEvent
	err := c.Bind(&eventRequest)
	if err != nil {
		return c.JSON(http.StatusBadRequest, errors.New("Invalid record"))
	}
	eventRequest.OwnerID = claims.IDentifier.Hex()
	data, err := event.CreateEvent(&eventRequest)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, nil)
	}
	return c.JSON(http.StatusOK, data)
}

//OwnEvents endpoint of GET method of  paged own event list
func OwnEvents(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	if user == nil {
		return c.JSON(http.StatusUnauthorized, errors.New("Authentication data is not correct. "))
	}
	claims := user.Claims.(*auth.JwtUserClaims)
	if claims == nil {
		return c.JSON(http.StatusForbidden, errors.New("User claims are absent. "))
	}

	page, err := strconv.ParseInt(c.QueryParam("page"), 10, 64)
	if err != nil {
		page = 0
	}

	data, err := event.OwnEvents(claims.IDentifier.Hex(), page)
	if err != nil {
		return c.JSON(http.StatusNotFound, nil)
	}
	return c.JSON(http.StatusOK, data)
}

//DeleteEvent endpoint of DELETE method of event deletion
func DeleteEvent(c echo.Context) error {

	user := c.Get("user").(*jwt.Token)
	if user == nil {
		return c.JSON(http.StatusUnauthorized, errors.New("Authentication data is not correct. "))
	}

	claims := user.Claims.(*auth.JwtUserClaims)
	if claims == nil {
		return c.JSON(http.StatusForbidden, errors.New("User claims are absent. "))
	}

	id := c.Param("id")

	if id == "" {
		return c.JSON(http.StatusBadRequest, errors.New("Event id is not defined. "))
	}

	err := event.DeleteEvent(claims.IDentifier.Hex(), id)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, errors.New("=( Oops, somthing went wrong"))
	}
	return c.NoContent(http.StatusOK)
}
