package main

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"

	"e/account/utils/config"

	auth "e/account/models/auth"

	eventEndPoints "e/account/endpoints/event"
	"e/account/messaging"
)

func main() {
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{config.GetDataBaseURL()},
		AllowMethods: []string{http.MethodPost, http.MethodDelete},
	}))

	jwtConfig := middleware.JWTConfig{
		Claims:     &auth.JwtUserClaims{},
		SigningKey: []byte(config.GetSecret()),
	}
	e.Use(middleware.JWTWithConfig(jwtConfig))

	// Routes
	events := e.Group("/event")
	events.POST("/", eventEndPoints.CreateEvent)
	events.GET("/own", eventEndPoints.OwnEvents)
	events.DELETE("/:id", eventEndPoints.DeleteEvent)

	//Initialization of producer, ques: deleted_events, created_events, files_to_delete
	//Initialization of consumers, ques: deleted_events, created_events
	messaging.Init()

	// Start server
	e.Logger.Fatal(e.Start(fmt.Sprintf(":%d", config.GetServerPort())))
}
