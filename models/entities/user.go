package entities

// User represent user data in database/users collection
type User struct {
	IDentifier  string   `bson:"_id"  json:"identifier"`
	AccessToken string   `bson:"accessToken"  json:"accessToken"`
	Setting     *Setting `bson:"setting"  json:"setting"`
}

// UserCachedData represent user request body
type UserCachedData struct {
	User *User `json:"user"`
}
