package entities

// Location represent place of the event
type Location struct {
	Name             string    `bson:"name"  json:"name"`
	DetailedLocation string    `bson:"detailedLocation"  json:"detailedLocation"`
	Type             string    `bson:"type"  json:"gType"`
	Coordinates      []float64 `bson:"coordinates"  json:"coordinates"`
	URL              string    `bson:"url"  json:"url"`
}
