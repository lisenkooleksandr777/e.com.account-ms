package entities

// Setting storage for user setting
type Setting struct {
	Range       int32 `bson:"range" json:"range"`
	DaysForward int32 `bson:"daysForward" json:"daysForward"`
}
