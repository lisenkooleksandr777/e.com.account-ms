package entities

//Follower ...
type Follower struct {
	UserID        string `json:"userId"`
	UserName      string `json:"userName"`
	AvatarURL     string `json:"avatarURL"`
	IsMyFollowing bool   `json:"isMyFollowing"`
}
