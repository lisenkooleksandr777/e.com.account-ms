package auth

import (
	jwt "github.com/dgrijalva/jwt-go"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

//JwtUserClaims is body of JWT token
type JwtUserClaims struct {
	IDentifier  primitive.ObjectID `json:"id"`
	Name        string             `json:"name"`
	PictureURL  string             `json:"pictureUrl"`
	Permissions []string           `json:"permissions"`
	jwt.StandardClaims
}
