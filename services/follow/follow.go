package follow

//GetFollowers ...
func GetFollowers(userID string, page int64) ([]*entities.Follower, error) {
	return neo4j.GetFollowers(userID, page*10, 10)
}

//GetFollowings ...
func GetFollowings(userID string, page int64) ([]*entities.Follower, error) {
	return neo4j.GetFollowings(userID, page*10, 10)
}
