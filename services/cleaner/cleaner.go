package cleaner

import (
	"e/account/messaging/producer"
	"e/account/models/entities"
	"e/account/repositories/graphsynch"
)

//DeleteEvent from the graph and remove all related resources to it
func DeleteEvent(event *entities.Event) error {
	err := graphsynch.DeleteEventFromGraph(event.ID.Hex())

	if err != nil {
		return err
	}

	err = producer.PublishFileToDeleteQue(event.LogoURL)
	if err != nil {
		//make some retry logic
	}

	return nil
}
