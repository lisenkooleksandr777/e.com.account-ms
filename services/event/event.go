package event

import (
	"e/account/messaging/producer"
	"e/account/models/entities"
	eventRepository "e/account/repositories/event"
)

// CreateEvent create new event and return it
func CreateEvent(request *entities.NewEvent) (*entities.Event, error) {

	event, err := eventRepository.Create(request)
	if err != nil {
		go func() {
			//add to rabbit as file that needs to be deleted
			err := producer.PublishFileToDeleteQue(request.LogoURL)
			if err != nil {
				//make some retry logic
			}
		}()
	} else {
		//adding record to que for events_created
		err = producer.PublishEventToCreatedQue(event)
		if err != nil {
			//do some retry logic here
		}
	}
	return event, err
}

// OwnEvents get next page with own events
func OwnEvents(ownerID string, page int64) ([]entities.PreviewEvent, error) {
	return eventRepository.OwnEvents(ownerID, page, 10)
}

// DeleteEvent create new event and return it
func DeleteEvent(ownerID string, eventID string) error {
	event, err := eventRepository.Delete(ownerID, eventID)
	if err == nil {
		//add to rabbit as file that needs to be deleted
		err := producer.PublishEventToDeletedQue(event)
		if err != nil {
			//make some retry logic
		}
	}
	return err
}
